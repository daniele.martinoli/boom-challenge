# ASSUMPTIONS
* 1 order can be assigned only to 1 photographer
* There are no requirements about system scale and redundancy, anyway it is designed with microservice architecture to
  take into account a future evolution
* Application security is also out of the scope, so all REST APIs are using unsecure HTTP requests
* All REST APIs are synchronous, and no events are raised by the inner services
* Since persistence is not required, entities are cached locally in the services, with a simple
  implementation of a data repository
* To simplify code review, I used the Lombok annotations to reduce the actual code to the bare minimum
* "business hours (8:00-20:00)" means 8:00 is fine but 20:00 is not

* Only R1 and R2 are implemented
* Also includes import endpoint to initialize photographs repository

# Application Architecture
* All services are based on Spring Boot REST stack
* Services connect through REST API
* Follows Database-per-service pattern
  
## Model component
* Library of POJO entities shared between the application services

## Order Application
* Exposes all the REST APIs to the consumers and dispatches to the proper service for 
  consumption, then returns the result (synch behavior)
* Acts as orchestrator/w Order and Photographer services
* References to other services are provided in application properties
* Port 8003

## Order Service
* Responsible to handle PhotoOrder
* Port 8001

## Photographer Service
* Responsible to handle Photographs
* Port 8002

# What would I have added if I only I had had time
* Service Discovery to register and lookup for active services
* API Gateway to dispatch external requests
* Persistence
  