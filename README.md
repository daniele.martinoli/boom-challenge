# Installating the application
## Requirements
* Java 11
* Maven 3.6
## Build instructions
Project is developed in Java (11) and available on my public git repo:
~~~~
git@gitlab.com:daniele.martinoli/boom-challenge.git
cd boom-challenge
mvn clean install
~~~~
# Starting services
Order service (HTTP 8001)
~~~~
mvn -f order-service/pom.xml spring-boot:run
~~~~
Photograph service (HTTP 8002)
~~~~
mvn -f photograph-service/pom.xml spring-boot:run
~~~~
Order app (HTTP 8003)
~~~~
mvn -f order-app/pom.xml spring-boot:run
~~~~

# Example of REST API
## Import photographs
~~~~
POST http://localhost:8003/import
POST http://localhost:8002/import
~~~~
~~~~
[{"name":"a"},{"name":"b"},{"name":"c"}]
~~~~
To test:
~~~~
GET http://localhost:8002/photographs
~~~~

## Create order
~~~~
POST http://localhost:8001/order
POST http://localhost:8003/order
~~~~
With mandatory fields
~~~~
{"name":"daniele", "surname":"martinoli", "email":"daniele.martinoli@gmail.com", "cellNumber":"
+391234567", "photoType":"Food"}
~~~~
Missing some mandatory fields
~~~~
{"surname":"martinoli", "email":"daniele.martinoli@gmail.com", "cellNumber":"
+391234567", "photoType":"Food"}
~~~~
~~~~
{"name":"daniele", "email":"daniele.martinoli@gmail.com", "cellNumber":"+391234567", "photoType":"Food"}
~~~~
~~~~
{"name":"daniele", "surname":"martinoli", "cellNumber":"+391234567", "photoType":"Food"}
~~~~
Examples of wrong dateAndTime
~~~~
"dateAndTime": "2021-01-17T20:00:00"
"dateAndTime": "2021-01-17T07:00:00"
~~~~
To test:
~~~~
GET http://localhost:8001/order
~~~~

## Schedule order
~~~~
PUT http://localhost:8003/order/11/schedule
"2021-01-17T20:00:00"
~~~~