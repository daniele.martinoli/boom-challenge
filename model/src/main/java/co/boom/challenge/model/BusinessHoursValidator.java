package co.boom.challenge.model;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.LocalDateTime;

public class BusinessHoursValidator implements ConstraintValidator<BusinessHours, LocalDateTime> {
    public void initialize(BusinessHours constraint) {
    }

    public boolean isValid(LocalDateTime dateTime, ConstraintValidatorContext context) {
        return dateTime == null || 8 <= dateTime.getHour() && dateTime.getHour() < 20;
    }
}