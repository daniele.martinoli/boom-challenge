package co.boom.challenge.model;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
public class PhotoOrder {
    private long id;
    @NotBlank(message = "Field name is mandatory")
    private String name;
    @NotBlank(message = "Field surname is mandatory")
    private String surname;
    @NotBlank(message = "Field email is mandatory")
    @Email(message = "Email must be valid")
    private String email;
    @NotBlank(message = "Field cellNumber is mandatory")
    private String cellNumber;
    @NotNull
    private PhotoType photoType;
    private String logisticInfo;
    @BusinessHours(message = "Field dataAndTime must be empty or time must be in 08:00-20:00 business hours")
    private LocalDateTime dateAndTime;

    private OrderState state;

    public enum OrderState {
        PENDING, UNSCHEDULED, ASSIGNED, UPLOADED, COMPLETED
    }

    public enum PhotoType {
        RealEstate, Food, Events
    }
}
