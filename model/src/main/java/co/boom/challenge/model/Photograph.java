package co.boom.challenge.model;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class Photograph {
    private long id;
    @NotBlank(message = "Field name is mandatory")
    private String name;
}
