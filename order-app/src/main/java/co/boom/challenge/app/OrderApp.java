package co.boom.challenge.app;

import co.boom.challenge.model.PhotoOrder;
import co.boom.challenge.model.Photograph;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SpringBootApplication
@RestController
@ComponentScan("co.boom.challenge.*")
public class OrderApp {
    @Value("${order-service.url}")
    private String orderServiceUrl;

    @Value("${photograph-service.url}")
    private String photographServiceUrl;

    public static void main(String[] args) {
        SpringApplication.run(OrderApp.class, args);
    }

    @PostMapping("/import")
    public ResponseEntity<List> importAll(@RequestBody List<Photograph> photographs) {
        RestTemplate restTemplate = new RestTemplate();
        RequestEntity<List<Photograph>> request = RequestEntity.post(photographServiceUrl + "/import")
                .contentType(MediaType.APPLICATION_JSON).body(photographs);

        ResponseEntity<List> response = restTemplate.exchange(request, List.class);
        return response;
    }

    @PostMapping("/orders")
    public ResponseEntity<?> newOrder(@Valid @RequestBody PhotoOrder orderToAdd) {
        RestTemplate restTemplate = new RestTemplate();
        RequestEntity<PhotoOrder> request = RequestEntity.post(orderServiceUrl + "/order")
                .contentType(MediaType.APPLICATION_JSON).body(orderToAdd);

        ResponseEntity<PhotoOrder> response = restTemplate.exchange(request, PhotoOrder.class);
        return response;
    }

    @PutMapping("/orders/{id}/schedule")
    public ResponseEntity<PhotoOrder> schedule(@PathVariable("id") long id, @RequestBody LocalDateTime scheduleDateAndTime) {
        RestTemplate restTemplate = new RestTemplate();
        RequestEntity<LocalDateTime> request = RequestEntity.put(orderServiceUrl + "/orders/" + id + "/schedule")
                .contentType(MediaType.APPLICATION_JSON).body(scheduleDateAndTime);

        ResponseEntity<PhotoOrder> response = restTemplate.exchange(request, PhotoOrder.class);
        return response;
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(
            MethodArgumentNotValidException ex) {
        var errors = new HashMap<String, String>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }
}