package co.boom.challenge.order;

import co.boom.challenge.model.PhotoOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class OrderController {
    @Autowired
    private OrderService orderService;

    @PostMapping("/orders")
    public ResponseEntity<PhotoOrder> newOrder(@Valid @RequestBody PhotoOrder orderToAdd) {
        return ResponseEntity.ok(orderService.newOrder(orderToAdd));
    }

    @PutMapping("/orders/{id}/schedule")
    public ResponseEntity<PhotoOrder> schedule(@PathVariable("id") long id, @RequestBody LocalDateTime scheduleDateAndTime) {
        return ResponseEntity.ok(orderService.schedule(id, scheduleDateAndTime));
    }

    @GetMapping("/order")
    public ResponseEntity<List<PhotoOrder>> allOrders() {
        return ResponseEntity.ok(orderService.fetchOrders());
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(
            MethodArgumentNotValidException ex) {
        var errors = new HashMap<String, String>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }
}