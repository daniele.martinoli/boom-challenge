package co.boom.challenge.order;


import co.boom.challenge.model.PhotoOrder;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
public class OrderRepository {
    private AtomicLong nextId = new AtomicLong();
    private List<PhotoOrder> orders = new ArrayList<>();

    public PhotoOrder save(PhotoOrder orderToSave) {
        if (orderToSave.getId() == 0) {
            orderToSave.setId(nextId.incrementAndGet());
            orders.add(orderToSave);
        } else {
            Optional<PhotoOrder> oldInstance = findById(orderToSave.getId());
            if (oldInstance.isPresent()) {
                orders.remove(oldInstance.get());
            }
            orders.add(orderToSave);
        }

        return orderToSave;
    }

    public List<PhotoOrder> findAll() {
        return List.copyOf(orders);
    }

    public Optional<PhotoOrder> findById(long id) {
        return orders.stream().filter(p -> p.getId() == id).findFirst();
    }
}
