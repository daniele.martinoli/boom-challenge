package co.boom.challenge.order;

import co.boom.challenge.model.PhotoOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static java.lang.String.format;

@Service
public class OrderService {
    @Autowired
    private OrderRepository orderRepository;

    public PhotoOrder newOrder(PhotoOrder orderToAdd) {
        if (Objects.isNull(orderToAdd.getDateAndTime())) {
            orderToAdd.setState(PhotoOrder.OrderState.UNSCHEDULED);
        } else {
            orderToAdd.setState(PhotoOrder.OrderState.PENDING);
        }
        return orderRepository.save(orderToAdd);
    }

    public List<PhotoOrder> fetchOrders() {
        return orderRepository.findAll();
    }

    public PhotoOrder schedule(long id, LocalDateTime scheduleDateAndTime) {
        Optional<PhotoOrder> orderToUpdate = orderRepository.findById(id);
        if (orderToUpdate.isEmpty()) {
            throw new IllegalArgumentException(format("No PhotoOrder with id %d", id));
        }
        if (PhotoOrder.OrderState.UNSCHEDULED.equals(orderToUpdate.get().getState())) {
            PhotoOrder updatedOrder = orderToUpdate.get();
            updatedOrder.setDateAndTime(scheduleDateAndTime);
            updatedOrder.setState(PhotoOrder.OrderState.PENDING);
            orderRepository.save(updatedOrder);
            return orderToUpdate.get();
        }
        throw new IllegalStateException(format("PhotoOrder %d in wrong state %s", orderToUpdate.get().getId(), orderToUpdate.get().getState()));
    }
}