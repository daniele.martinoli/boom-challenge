package co.boom.challenge.order;

import co.boom.challenge.model.PhotoOrder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.notNullValue;


@SpringBootTest
@ContextConfiguration(classes = {OrderServiceApp.class})
@AutoConfigureMockMvc
class OrderControllerTest {
    @MockBean
    private OrderService orderService;

    @Autowired
    private OrderController orderController;

    @Autowired
    private MockMvc mockMvc;

    @BeforeEach
    public void setup() {
        Mockito.when(orderService.newOrder(Mockito.any(PhotoOrder.class))).thenAnswer(new Answer<PhotoOrder>() {
            @Override
            public PhotoOrder answer(InvocationOnMock invocationOnMock) throws Throwable {
                return invocationOnMock.getArgument(0);
            }
        });
    }

    @Test
    public void contextLoads() throws Exception {
        assertThat(orderController, notNullValue());
    }

    @Test
    void whenOrderIsValid_then200isReturned() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/orders")
                .content("{\"name\":\"daniele\", \"surname\":\"martinoli\", \"email\":\"daniele.martinoli@gmail.com\", \"cellNumber\":" +
                        "\"+391234567\", \"photoType\":\"Food\"}")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    void whenOrderIsNot_then500isReturned() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/orders")
                .content("{\"name\":\"\", \"surname\":\"martinoli\", \"email\":\"daniele.martinoli@gmail.com\", \"cellNumber\":" +
                        "\"+391234567\", \"photoType\":\"Food\"}")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().string(containsString("name is mandatory")))
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON));
    }
}