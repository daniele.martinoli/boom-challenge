package co.boom.challenge.order;

import co.boom.challenge.model.PhotoOrder;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ExtendWith(SpringExtension.class)
public class OrderServiceTest {
    @Autowired
    private OrderService orderService;

    @Test
    public void whenOrderIsAdded_andDateTimeIsNull_thenStateIsUnscheduled() {
        PhotoOrder order = new PhotoOrder();
        order.setName("name");
        order.setSurname("surname");
        order.setEmail("email@mail.com");
        order.setCellNumber("1234");
        order.setPhotoType(PhotoOrder.PhotoType.Food);

        PhotoOrder result = orderService.newOrder(order);

        assertNotNull(result);
        assertTrue(result.getId() > 0);
        assertEquals(order.getName(), result.getName());
        assertEquals(PhotoOrder.OrderState.UNSCHEDULED, result.getState());
    }

    @Test
    public void whenOrderIsAdded_andDateTimeIsNotNull_thenStateIsPending() {
        PhotoOrder order = new PhotoOrder();
        order.setName("name");
        order.setSurname("surname");
        order.setEmail("email@mail.com");
        order.setCellNumber("1234");
        order.setPhotoType(PhotoOrder.PhotoType.Food);
        order.setDateAndTime(LocalDateTime.now().withHour(9));

        PhotoOrder result = orderService.newOrder(order);

        assertNotNull(result);
        assertTrue(result.getId() > 0);
        assertEquals(order.getName(), result.getName());
        assertEquals(PhotoOrder.OrderState.PENDING, result.getState());
    }

    @Test
    public void whenOrderIsAdded_andDateTimeIsNull_andThenIsScheduled_thenStateIsPending() {
        PhotoOrder order = new PhotoOrder();
        order.setName("name");
        order.setSurname("surname");
        order.setEmail("email@mail.com");
        order.setCellNumber("1234");
        order.setPhotoType(PhotoOrder.PhotoType.Food);

        PhotoOrder initialOrder = orderService.newOrder(order);
        PhotoOrder scheduledOrder = orderService.schedule(initialOrder.getId(), LocalDateTime.now().withHour(9));

        assertNotNull(scheduledOrder);
        assertNotNull(scheduledOrder.getDateAndTime());
        assertEquals(initialOrder.getId(), scheduledOrder.getId());
        assertEquals(order.getName(), scheduledOrder.getName());
        assertEquals(PhotoOrder.OrderState.PENDING, scheduledOrder.getState());
    }

    @Test
    public void whenOrderIsAdded_andDateTimeIsNotNull_andThenIsScheduled_thenThrowException() {
        PhotoOrder order = new PhotoOrder();
        order.setName("name");
        order.setSurname("surname");
        order.setEmail("email@mail.com");
        order.setCellNumber("1234");
        order.setPhotoType(PhotoOrder.PhotoType.Food);
        order.setDateAndTime(LocalDateTime.now().withHour(9));

        PhotoOrder initialOrder = orderService.newOrder(order);
        assertThrows(IllegalStateException.class, () -> orderService.schedule(initialOrder.getId(), LocalDateTime.now().withHour(9)));
    }
}
