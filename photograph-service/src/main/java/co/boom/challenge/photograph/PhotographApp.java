package co.boom.challenge.photograph;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("co.boom.challenge.*")
public class PhotographApp {

    public static void main(String[] args) {
        SpringApplication.run(PhotographApp.class, args);
    }
}