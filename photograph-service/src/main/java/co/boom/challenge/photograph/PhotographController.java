package co.boom.challenge.photograph;

import co.boom.challenge.model.Photograph;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class PhotographController {
    @Autowired
    private PhotographService photographService;

    @PostMapping("/import")
    public ResponseEntity<List<Photograph>> importAll(@RequestBody List<Photograph> photographs) {
        return ResponseEntity.ok(photographService.importAll(photographs));
    }

    @GetMapping("/photographs")
    public ResponseEntity<List<Photograph>> allPhotographs() {
        return ResponseEntity.ok(photographService.fetchPhotographs());
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(
            MethodArgumentNotValidException ex) {
        var errors = new HashMap<String, String>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }
}