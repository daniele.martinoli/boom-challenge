package co.boom.challenge.photograph;


import co.boom.challenge.model.Photograph;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
public class PhotographRepository {
    private AtomicLong nextId = new AtomicLong();
    private List<Photograph> photographs = new ArrayList<>();

    public List<Photograph> saveAll(List<Photograph> photographs) {
        photographs.forEach(photograph -> photograph.setId(nextId.incrementAndGet()));
        this.photographs.addAll(photographs);
        return photographs;
    }

    public List<Photograph> findAll() {
        return List.copyOf(photographs);
    }
}
