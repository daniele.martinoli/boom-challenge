package co.boom.challenge.photograph;

import co.boom.challenge.model.Photograph;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PhotographService {
    @Autowired
    private PhotographRepository photographRepository;

    public List<Photograph> importAll(List<Photograph> photographs) {
        return photographRepository.saveAll(photographs);
    }

    public List<Photograph> fetchPhotographs() {
        return photographRepository.findAll();
    }
}