package co.boom.challenge.photograph;

import co.boom.challenge.model.Photograph;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;
import java.util.stream.IntStream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.notNullValue;


@SpringBootTest
@ContextConfiguration(classes = {PhotographApp.class})
@AutoConfigureMockMvc
class PhotographControllerTest {
    @MockBean
    private PhotographService photographService;

    @Autowired
    private PhotographController photographController;

    @Autowired
    private MockMvc mockMvc;

    @BeforeEach
    public void setup() {
        Mockito.when(photographService.importAll(Mockito.any(List.class))).thenAnswer(new Answer<List<Photograph>>() {
            @Override
            public List<Photograph> answer(InvocationOnMock invocationOnMock) throws Throwable {
                return invocationOnMock.getArgument(0);
            }
        });
    }

    @Test
    public void contextLoads() throws Exception {
        assertThat(photographController, notNullValue());
    }

    @Test
    void whenOrderIsValid_then200isReturned() throws Exception {
        StringBuilder sb = new StringBuilder("[");
        IntStream.rangeClosed(1, 10).forEach(i -> {
            sb.append("{\"name\":\"name" + i + "\", \"surname\":\"surname\", \"email\":\"email@mail.com\", \"cellNumber\":" +
                    "\"+391234567\"}");
            if (i < 10) {
                sb.append((",\n"));
            }
        });
        sb.append("]");

        mockMvc.perform(MockMvcRequestBuilders.post("/import")
                .content(sb.toString())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON));
    }
}