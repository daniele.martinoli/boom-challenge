package co.boom.challenge.photograph;

import co.boom.challenge.model.Photograph;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
@ExtendWith(SpringExtension.class)
public class PhotographServiceTest {

    @Autowired
    private PhotographService photographService;

    @Autowired
    private PhotographRepository photographRepository;

    @Test
    public void whenPhotographIsAdded_thenIdIsInitialized() {
        List<Photograph> photographs = new ArrayList<>();
        IntStream.rangeClosed(1, 10).forEach(i -> {
            Photograph photograph = new Photograph();
            photograph.setName("name" + i);
            photographs.add(photograph);
        });

        List<Photograph> result = photographService.importAll(photographs);

        assertNotNull(result);
        assertEquals(10, result.size());
        result.forEach(photograph -> assertNotNull(photograph.getId()));
    }
}
